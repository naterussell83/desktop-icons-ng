# Finnish translation for desktop-icons.
# Copyright (C) 2018-2022 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
#
# Jiri Grönroos <jiri.gronroos@iki.fi>, 2018.
# Timo Jyrinki <timo.jyrinki+l10n@gmail.com>, 2021.
# Tuomas Nurmi <tuomas.nurmi@opinsys.fi>, 2022.
# Weblate Translation Memory <noreply-mt-weblate-translation-memory@weblate.org>, 2023, 2024.
# Weblate <noreply-mt-weblate@weblate.org>, 2023, 2024.
# LibreTranslate <noreply-mt-libretranslate@weblate.org>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: gtk4-ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-07-04 10:36-0400\n"
"PO-Revision-Date: 2024-07-03 00:09+0000\n"
"Last-Translator: Weblate Translation Memory <noreply-mt-weblate-translation-"
"memory@weblate.org>\n"
"Language-Team: Finnish <https://hosted.weblate.org/projects/gtk4-desktop-"
"icons-ng/gtk4-ding-pot/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.7-dev\n"
"X-Launchpad-Export-Date: 2021-04-08 06:16+0000\n"

#: app/adwPreferencesWindow.js:175 app/adwPreferencesWindow.js:176
#, fuzzy
msgid "Desktop"
msgstr "Työpöytä"

#: app/adwPreferencesWindow.js:180 app/adwPreferencesWindow.js:181
#, fuzzy
msgid "Files"
msgstr "Tiedostot"

#: app/adwPreferencesWindow.js:185 app/adwPreferencesWindow.js:186
#: app/adwPreferencesWindow.js:216
#, fuzzy
msgid "Tweaks"
msgstr "Säädöt"

#: app/adwPreferencesWindow.js:190 app/adwPreferencesWindow.js:191
#, fuzzy
msgid "About"
msgstr "Tietoja"

#: app/adwPreferencesWindow.js:201
#, fuzzy
msgid "Desktop Settings"
msgstr "Työpöytäasetukset"

#: app/adwPreferencesWindow.js:202
#, fuzzy
msgid "Settings for the Desktop Program"
msgstr "Asetukset työpöytäohjelmaan"

#: app/adwPreferencesWindow.js:206
#, fuzzy
msgid "Volumes"
msgstr "Taltiot"

#: app/adwPreferencesWindow.js:207
#, fuzzy
msgid "Desktop volumes display"
msgstr "Työpöydän tilavuusnäytöt"

#: app/adwPreferencesWindow.js:211
#, fuzzy
msgid "Files Settings"
msgstr "Asetukset Files"

#: app/adwPreferencesWindow.js:212
#, fuzzy
msgid "Settings shared with Gnome Files"
msgstr "Asetukset jaettu Gnome Files"

#: app/adwPreferencesWindow.js:217
#, fuzzy
msgid "Miscellaneous Tweaks"
msgstr "Väärä Tweet"

#: app/adwPreferencesWindow.js:228
msgid "Size for the desktop icons"
msgstr "Työpöytäkuvakkeiden koko"

#: app/adwPreferencesWindow.js:230
msgid "Tiny"
msgstr "Erittäin pieni"

#: app/adwPreferencesWindow.js:231
msgid "Small"
msgstr "Pieni"

#: app/adwPreferencesWindow.js:232
msgid "Standard"
msgstr "Tavallinen"

#: app/adwPreferencesWindow.js:233
msgid "Large"
msgstr "Suuri"

#: app/adwPreferencesWindow.js:238
msgid "New icons alignment"
msgstr "Uusien kuvakkeiden asettelu"

#: app/adwPreferencesWindow.js:240
#, fuzzy
msgid "Top left corner"
msgstr "Vasen yläkulma"

#: app/adwPreferencesWindow.js:241
#, fuzzy
msgid "Top right corner"
msgstr "Oikea yläkulma"

#: app/adwPreferencesWindow.js:242
#, fuzzy
msgid "Bottom left corner"
msgstr "Vasen alakulma"

#: app/adwPreferencesWindow.js:243
#, fuzzy
msgid "Bottom right corner"
msgstr "Oikea alakulma"

#: app/adwPreferencesWindow.js:248
#, fuzzy
msgid "Add new icons to Secondary Monitors first, if available"
msgstr ""
"Lisätään uudet kuvakkeet ensin toissijaisiin monitoreihin, jos niitä on "
"saatavilla"

#: app/adwPreferencesWindow.js:252
#, fuzzy
msgid "Show the personal folder on the desktop"
msgstr "Näytä kotikansio työpöydällä"

#: app/adwPreferencesWindow.js:256
#, fuzzy
msgid "Show the trash icon on the desktop"
msgstr "Näytä roskakorin kuvake työpöydällä"

#: app/adwPreferencesWindow.js:260
#, fuzzy
msgid "Show external drives on the desktop"
msgstr "Näytä erilliset tallennusvälineet työpöydällä"

#: app/adwPreferencesWindow.js:264
#, fuzzy
msgid "Show network drives on the desktop"
msgstr "Näytä verkkoasemat työpöydällä"

#: app/adwPreferencesWindow.js:268
#, fuzzy
msgid "Add new drives to the opposite side of the desktop"
msgstr "Lisää uudet tallennusvälineet näytön toiselle puolelle"

#: app/adwPreferencesWindow.js:273
#, fuzzy
msgid "Snap icons to grid"
msgstr "Snap ikonit verkkoon"

#: app/adwPreferencesWindow.js:278
#, fuzzy
msgid "Highlight the drop grid"
msgstr "Kohota pisaraverkko"

#: app/adwPreferencesWindow.js:285
msgid "Add an emblem to soft links"
msgstr "Merkitse linkit tiedostoihin symbolilla"

#: app/adwPreferencesWindow.js:288
msgid "Use dark text in icon labels"
msgstr "Käytä tummaa tekstiä kuvakkeiden selitteissä"

#: app/adwPreferencesWindow.js:293
#, fuzzy
msgid "Action to Open Items"
msgstr "Toiminta avoimiin kohteisiin"

#: app/adwPreferencesWindow.js:295
msgid "Single click"
msgstr "Yksi napsautus"

#: app/adwPreferencesWindow.js:296
msgid "Double click"
msgstr "Kaksoisnapsautus"

#: app/adwPreferencesWindow.js:300
msgid "Show image thumbnails"
msgstr "Näytä pienoiskuvat"

#: app/adwPreferencesWindow.js:302
msgid "Always"
msgstr "Aina"

#: app/adwPreferencesWindow.js:303
#, fuzzy
msgid "On this computer only"
msgstr "Vain tällä tietokoneella"

#: app/adwPreferencesWindow.js:304
msgid "Never"
msgstr "Ei koskaan"

#: app/adwPreferencesWindow.js:308
msgid "Show a context menu item to delete permanently"
msgstr "Näytä valikkokohde pysyvästi poistamiselle"

#: app/adwPreferencesWindow.js:312
msgid "Show hidden files"
msgstr "Näytä piilotiedostot"

#: app/adwPreferencesWindow.js:316
#, fuzzy
msgid "Open folders on drag hover"
msgstr "Avoimet kansiot drag hoverissa"

#: app/adwPreferencesWindow.js:319
#, fuzzy
msgid "Website"
msgstr "Web-sivusto"

#: app/adwPreferencesWindow.js:321
#, fuzzy
msgid "Visit"
msgstr "Käynti"

#: app/adwPreferencesWindow.js:324
#, fuzzy
msgid "Issues"
msgstr "Ongelmat"

#: app/adwPreferencesWindow.js:325
#, fuzzy
msgid "Report issues on issue tracker"
msgstr "Raportti aiheista seuraaja"

#: app/adwPreferencesWindow.js:326
#, fuzzy
msgid "Report"
msgstr "Raportoi"

#: app/adwPreferencesWindow.js:328
#, fuzzy
msgid "License"
msgstr "Lisenssi"

#: app/adwPreferencesWindow.js:333
#, fuzzy
msgid "Translation"
msgstr "Käännös"

#: app/adwPreferencesWindow.js:334
#, fuzzy
msgid "Help translate in your web browser"
msgstr "Auta kääntämään verkkoselaimesi"

#: app/adwPreferencesWindow.js:335
#, fuzzy
msgid "Translate"
msgstr "Käännä"

#: app/appChooser.js:54
#, fuzzy
msgid "Choose an application to open <b>{foo}</b>"
msgstr "Valitse hakemus avataksesi <b> </b>"

#: app/appChooser.js:62
#, fuzzy
msgid "Open Items"
msgstr "Avaa kohde"

#: app/appChooser.js:64
#, fuzzy
msgid "Open Folder"
msgstr "Avaa kansio"

#: app/appChooser.js:66 app/resources/ui/ding-app-chooser.ui:6
#, fuzzy
msgid "Open File"
msgstr "Avaa tiedosto"

#: app/appChooser.js:106
#, fuzzy
msgid "Error changing default application"
msgstr "Väärin muuttuva oletussovellus"

#: app/appChooser.js:107
#, fuzzy
msgid "Error while setting {foo} as default application for {mimetype}"
msgstr "Virhe asettamalla {foo} oletussovelluksena {mimetype}"

#: app/askRenamePopup.js:49
msgid "Folder name"
msgstr "Kansion nimi"

#: app/askRenamePopup.js:49
msgid "File name"
msgstr "Tiedoston nimi"

#: app/askRenamePopup.js:57 app/autoAr.js:299 app/desktopManager.js:1243
#: app/fileItemMenu.js:805
msgid "OK"
msgstr "OK"

#: app/askRenamePopup.js:57
msgid "Rename"
msgstr "Nimeä uudelleen"

#: app/autoAr.js:71
#, fuzzy
msgid "AutoAr is not installed"
msgstr "Auto Auto Auto Auto Auto Auto Auto Auto A ei ole asennettu"

#: app/autoAr.js:72
#, fuzzy
msgid ""
"To be able to work with compressed files, install file-roller and/or gir-1.2-"
"gnomeAutoAr"
msgstr ""
"Jotta voit työskennellä pakattujen tiedostojen kanssa, asenna tiedostorulla "
"ja/tai gir-1.2-gnome Auto"

#: app/autoAr.js:203
#, fuzzy
msgid "Extracting files"
msgstr "Poista tiedostoja"

#: app/autoAr.js:220
#, fuzzy
msgid "Compressing files"
msgstr "Pakataan tiedostoja."

#: app/autoAr.js:291 app/autoAr.js:620 app/desktopManager.js:773
#: app/desktopManager.js:1245 app/fileItemMenu.js:651 app/fileItemMenu.js:805
#: app/showErrorPopup.js:39 app/showErrorPopup.js:43
msgid "Cancel"
msgstr "Peru"

#: app/autoAr.js:311 app/autoAr.js:608
#, fuzzy
msgid "Enter a password here"
msgstr "Kirjoita salasana tänne"

#: app/autoAr.js:354
#, fuzzy
msgid "Removing partial file '${outputFile}'"
msgstr "Poista osittainen tiedosto \"outputfiili\" \"\"\""

#: app/autoAr.js:373
#, fuzzy
msgid "Creating destination folder"
msgstr "Kohdekansion luominen"

#: app/autoAr.js:405
#, fuzzy
msgid "Extracting files into '${outputPath}'"
msgstr "Tiedostojen uudistaminen \"OutputPath\" \"\"\""

#: app/autoAr.js:435
#, fuzzy
msgid "Extraction completed"
msgstr "Loppusijoitus"

#: app/autoAr.js:436
#, fuzzy
msgid "Extracting '${fullPathFile}' has been completed."
msgstr "\"Täydelliset lyhenteet\" on saatu valmiiksi."

#: app/autoAr.js:442
#, fuzzy
msgid "Extraction cancelled"
msgstr "Poikkeaminen peruttu"

#: app/autoAr.js:443
#, fuzzy
msgid "Extracting '${fullPathFile}' has been cancelled by the user."
msgstr "Käyttäjä on perunut \"fullPathFile\" uudistamisen."

#: app/autoAr.js:453
#, fuzzy
msgid "Passphrase required for ${filename}"
msgstr "Passfraasi, joka tarvitaan filename"

#: app/autoAr.js:456
#, fuzzy
msgid "Error during extraction"
msgstr "Virheet poikkeuksen aikana"

#: app/autoAr.js:483
#, fuzzy
msgid "Compressing files into '${outputFile}'"
msgstr "Tiedostojen painaminen \"outputfiiliin\" \"\"\""

#: app/autoAr.js:496
#, fuzzy
msgid "Compression completed"
msgstr "Kompressio valmis"

#: app/autoAr.js:497
#, fuzzy
msgid "Compressing files into '${outputFile}' has been completed."
msgstr "Tiedostojen pakkaaminen \"outputfiiliin\" on päättynyt."

#: app/autoAr.js:501 app/autoAr.js:508
#, fuzzy
msgid "Cancelled compression"
msgstr "Peruutettu puristus"

#: app/autoAr.js:502
#, fuzzy
msgid "The output file '${outputFile}' already exists."
msgstr "Tuotetiedosto \"outputfiili\" on jo olemassa."

#: app/autoAr.js:509
#, fuzzy
msgid "Compressing files into '${outputFile}' has been cancelled by the user."
msgstr ""
"Tiedostojen painaminen \"outputfiiliin\" on peruutettu käyttäjän toimesta."

#: app/autoAr.js:512
#, fuzzy
msgid "Error during compression"
msgstr "Virhe kompression aikana"

#: app/autoAr.js:547
#, fuzzy
msgid "Create archive"
msgstr "Luo arkisto"

#: app/autoAr.js:571
#, fuzzy
msgid "Archive name"
msgstr "Archive Nimi"

#: app/autoAr.js:604
#, fuzzy
msgid "Password"
msgstr "Salasana"

#: app/autoAr.js:617
#, fuzzy
msgid "Create"
msgstr "Luo"

#: app/autoAr.js:688
#, fuzzy
msgid "Compatible with all operating systems."
msgstr "Yhteensopiva kaikkien käyttöjärjestelmien kanssa."

#: app/autoAr.js:694
#, fuzzy
msgid "Password protected .zip, must be installed on Windows and Mac."
msgstr "Password protected .zip on asennettava Windowsiin ja Maciin."

#: app/autoAr.js:700
#, fuzzy
msgid "Smaller archives but Linux and Mac only."
msgstr "Pienempiä arkistoja, mutta vain Linux ja Mac."

#: app/autoAr.js:706
#, fuzzy
msgid "Smaller archives but must be installed on Windows and Mac."
msgstr "Pienempiä arkistoja, mutta ne on asennettava Windowsiin ja Maciin."

#: app/desktopGrid.js:47
#, fuzzy
msgid "Desktop Icons"
msgstr "Työpöytä Icons"

#: app/desktopManager.js:139
#, fuzzy
msgid "GNOME Files not found"
msgstr "GNOME Tiedostoja ei löytynyt"

#: app/desktopManager.js:140
#, fuzzy
msgid "The GNOME Files application is required by Gtk4 Desktop Icons NG."
msgstr "GNOME Tiedostojen käyttö edellyttää Gtk4 Desktop Icons NG."

#: app/desktopManager.js:152
#, fuzzy
msgid "There is no default File Manager"
msgstr "Epäonnistunutta tiedostopäällikköä ei ole"

#: app/desktopManager.js:153
#, fuzzy
msgid "There is no application that handles mimetype \"inode/directory\""
msgstr ""
"Ei ole olemassa hakemusta, joka käsittelee mimetypiaa \"inode/directory\""

#: app/desktopManager.js:165
#, fuzzy
msgid "Gnome Files is not registered as a File Manager"
msgstr "Gnome Files ei ole rekisteröity tiedostojen johtajaksi"

#: app/desktopManager.js:166
#, fuzzy
msgid ""
"The Gnome Files application is not programmed to open Folders!\n"
"Check your xdg-utils installation\n"
"Check Gnome Files .desktop File installation"
msgstr ""
"Gnome Files -sovellusta ei ole ohjelmoitu avaamaan kansioita.\n"
"Tarkista xdg-käyttöjärjestelmäsi asennus\n"
"Katso Gnome Files.desktop Tiedoston asennus"

#: app/desktopManager.js:772
#, fuzzy
msgid "Choose Action for Files"
msgstr "Valitse tiedostojen toiminta"

#: app/desktopManager.js:773
#, fuzzy
msgid "Move"
msgstr "Siirrä"

#: app/desktopManager.js:773 app/fileItemMenu.js:372
msgid "Copy"
msgstr "Kopioi"

#: app/desktopManager.js:773
#, fuzzy
msgid "Link"
msgstr "Linkki"

#: app/desktopManager.js:860 app/desktopManager.js:892
#, fuzzy
msgid "Making SymLink Failed"
msgstr "SymLink epäonnistui"

#: app/desktopManager.js:861 app/desktopManager.js:893
#, fuzzy
msgid "Could not create symbolic link"
msgstr "Ei voi luoda symbolista linkkiä"

#: app/desktopManager.js:1203
#, fuzzy
msgid "Clear current selection before new search"
msgstr "Tyhjennä nykyinen valinta ennen uutta hakua"

#: app/desktopManager.js:1247
msgid "Find Files on Desktop"
msgstr "Etsi tiedostoja työpöydällä"

#: app/desktopManager.js:1510
#, fuzzy
msgid "Name"
msgstr "Nimi"

#: app/desktopManager.js:1511
#, fuzzy
msgid "Name Z-A"
msgstr "Nimi (Z-A)"

#: app/desktopManager.js:1512
#, fuzzy
msgid "Modified Time"
msgstr "Muutettu aika"

#: app/desktopManager.js:1513
#, fuzzy
msgid "Type"
msgstr "Tyyppi"

#: app/desktopManager.js:1514
#, fuzzy
msgid "Size"
msgstr "Koko"

#: app/desktopManager.js:1517
#, fuzzy
msgid "Keep Arranged…"
msgstr "Pidä järjestettynä…"

#: app/desktopManager.js:1521
#, fuzzy
msgid "Keep Stacked by Type…"
msgstr "Pidä pinottuna tyypin mukaan…"

#: app/desktopManager.js:1522
#, fuzzy
msgid "Sort Home/Drives/Trash…"
msgstr "Järjestä koti/asemat/roskakori…"

#: app/desktopManager.js:1527 app/desktopManager.js:2578
msgid "New Folder"
msgstr "Uusi kansio"

#: app/desktopManager.js:1531
msgid "New Document"
msgstr "Uusi asiakirja"

#: app/desktopManager.js:1535
msgid "Paste"
msgstr "Liitä"

#: app/desktopManager.js:1536
msgid "Undo"
msgstr "Kumoa"

#: app/desktopManager.js:1537
msgid "Redo"
msgstr "Tee uudelleen"

#: app/desktopManager.js:1542
msgid "Select All"
msgstr "Valitse kaikki"

#: app/desktopManager.js:1547
msgid "Arrange Icons"
msgstr "Järjestä kuvakkeet"

#: app/desktopManager.js:1551
#, fuzzy
msgid "Arrange By…"
msgstr "Järjestäytyminen.."

#: app/desktopManager.js:1557
#, fuzzy
#| msgid "Show Desktop in Files"
msgid "Show Desktop In {0}"
msgstr "Näytä työpöytä {0}"

#: app/desktopManager.js:1560
#, fuzzy
msgid "Open In {0}"
msgstr "Avoinna {0}"

#: app/desktopManager.js:1566
#, fuzzy
msgid "Desktop Icon Settings"
msgstr "Työpöytäkuvakkeiden asetukset"

#: app/desktopManager.js:1571
msgid "Shell Menu…"
msgstr ""

#: app/desktopManager.js:1617
#, fuzzy
msgid "Preferences Window is Open"
msgstr "Ikkuna on auki"

#: app/desktopManager.js:1617
#, fuzzy
msgid "This Window is open. Please switch to the active window."
msgstr "Tämä ikkuna on auki. Siirry aktiiviseen ikkunaan."

#: app/desktopManager.js:1631
msgid "Settings"
msgstr "Asetukset"

#: app/desktopManager.js:2601
#, fuzzy
msgid "Folder Creation Failed"
msgstr "Folder Creation epäonnistui"

#: app/desktopManager.js:2602
#, fuzzy
msgid "Could not create folder"
msgstr "Ei voitu luoda kansiota"

#: app/desktopManager.js:2645
#, fuzzy
msgid "Template Creation Error"
msgstr "Templata Creation Error"

#: app/desktopManager.js:2646
#, fuzzy
msgid "Could not create document"
msgstr "Asiakirjaa ei voi luoda"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: app/fileItem.js:138 app/fileItem.js:147
msgid "Home"
msgstr "Koti"

#: app/fileItem.js:153
#, fuzzy
#| msgid "Empty Trash"
msgid "Trash"
msgstr "Roskakori"

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * an external drive is selected. Example: if a USB stick named "my_portable"
#. * is selected, it will say "Drive my_portable"
#: app/fileItem.js:159
#, fuzzy
msgid "Drive"
msgstr "Ajaminen"

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * a folder is selected. Example: if a folder named "things" is selected,
#. * it will say "Folder things"
#: app/fileItem.js:169
#, fuzzy
#| msgid "New Folder"
msgid "Folder"
msgstr "Kansio"

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * a normal file is selected. Example: if a file named "my_picture.jpg"
#. * is selected, it will say "File my_picture.jpg"
#: app/fileItem.js:176
#, fuzzy
msgid "File"
msgstr "Tiedosto"

#: app/fileItem.js:305
msgid "Broken Link"
msgstr "Rikkinäinen linkki"

#: app/fileItem.js:306
msgid "Can not open this File because it is a Broken Symlink"
msgstr "Tiedostoa ei voitu avata, sillä se on rikkinäinen linkki"

#: app/fileItem.js:337
#, fuzzy
msgid "Opening File Failed"
msgstr "Tiedoston avaaminen epäonnistui"

#: app/fileItem.js:339
#, fuzzy
msgid "There is no application installed to open \"{foo}\" files."
msgstr "Mitään sovellusta ei ole asennettu avaamaan tiedostoja."

#: app/fileItem.js:368
msgid "Broken Desktop File"
msgstr "Rikkinäinen työpöytätiedosto"

#: app/fileItem.js:369
#, fuzzy
#| msgid ""
#| "This .desktop file has errors or points to a program without permissions. "
#| "It can not be executed.\n"
#| "\n"
#| "\t<b>Edit the file to set the correct executable Program.</b>"
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"Edit the file to set the correct executable Program."
msgstr ""
"Tässä .desktop-tiedostossa on virheitä, tai se osoittaa ohjelmaan, jolla on "
"puutteelliset oikeudet. Sitä ei voi suorittaa.\n"
"\n"
"\t<b>Muokkaa tiedostoa asettaaksesi oikea suoritettava ohjelma.<b>"

#: app/fileItem.js:375
msgid "Invalid Permissions on Desktop File"
msgstr "Vialliset oikeudet työpöytätiedostossa"

#: app/fileItem.js:376
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""
"Tällä .desktop-tiedostolla on väärät oikeudet. Naksauta oikealla hiiren "
"napilla ja valitse Ominaisuudet, sitten:\n"

#: app/fileItem.js:378
#, fuzzy
#| msgid ""
#| "\n"
#| "<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgid ""
"\n"
"Set Permissions, in \"Others Access\", \"Read Only\" or \"None\""
msgstr ""
"\n"
"<b>Aseta oikeudet kohdassa \"Muut oikeudet\", joko \"Vain luku\" tai \"Ei "
"mikään\"</b>"

#: app/fileItem.js:381
#, fuzzy
#| msgid ""
#| "\n"
#| "<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgid ""
"\n"
"Enable option, \"Allow Executing File as a Program\""
msgstr ""
"\n"
"<b>Kytke päälle valinta \"Salli tiedoston suoritus ohjelmana\"</b>"

#: app/fileItem.js:388
#, fuzzy
#| msgid "Broken Desktop File"
msgid "Untrusted Desktop File"
msgstr "Luottamaton työpöytätiedosto"

#: app/fileItem.js:389
#, fuzzy
#| msgid ""
#| "This .desktop file is not trusted, it can not be launched. To enable "
#| "launching, right-click, then:\n"
#| "\n"
#| "<b>Enable \"Allow Launching\"</b>"
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"Enable \"Allow Launching\""
msgstr ""
"Tämä .desktop-tiedosto ei ole luotettu, ja sitä ei voi käynnistää. "
"Salliakseksi käynnistämisen naksauta hiiren oikealla napilla, sitten:\n"
"\n"
"<b>Valitse \"Salli käynnistäminen\"</b>"

#: app/fileItem.js:399 app/gnomeShellDragDrop.js:238
#, fuzzy
msgid "Could not open File"
msgstr "Tiedostoa ei voitu avata"

#. eslint-disable-next-line no-template-curly-in-string
#: app/fileItem.js:401 app/gnomeShellDragDrop.js:240
#, fuzzy
msgid "${appName} can not open files of this Type!"
msgstr "tiedostoja ei voi avata tämän tyypin tiedostoja!"

#: app/fileItemMenu.js:298
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Uusi kansio jossa {0} kohde"
msgstr[1] "Uusi kansio jossa {0} kohdetta"

#: app/fileItemMenu.js:305
msgid "Open All..."
msgstr "Avaa kaikki..."

#: app/fileItemMenu.js:316
#, fuzzy
msgid "Run"
msgstr "Suorita"

#: app/fileItemMenu.js:318
#, fuzzy
msgid "Open with {foo}"
msgstr "Avoinna {foo}"

#: app/fileItemMenu.js:320
msgid "Open"
msgstr "Avaa"

#: app/fileItemMenu.js:329 app/fileItemMenu.js:333
msgid "Extract Here"
msgstr "Pura tänne"

#: app/fileItemMenu.js:335
msgid "Extract To..."
msgstr "Pura…"

#: app/fileItemMenu.js:340 app/fileItemMenu.js:343
#, fuzzy
msgid "Open With..."
msgstr "Avaa sovelluksella..."

#: app/fileItemMenu.js:343
msgid "Open All With Other Application..."
msgstr "Avaa kaikki toisella sovelluksella..."

#: app/fileItemMenu.js:346
msgid "Launch using Dedicated Graphics Card"
msgstr "Käynnistä erillisnäytönohjainta käyttäen"

#: app/fileItemMenu.js:353
msgid "Stack This Type"
msgstr "Pinoa tämäntyyppiset"

#: app/fileItemMenu.js:353
msgid "Unstack This Type"
msgstr "Älä pinoa tämäntyyppisiä"

#: app/fileItemMenu.js:364
#, fuzzy
msgid "Run as a Program"
msgstr "Suorita ohjelmana"

#: app/fileItemMenu.js:367
msgid "Scripts"
msgstr "Komentosarjat"

#: app/fileItemMenu.js:370
msgid "Cut"
msgstr "Leikkaa"

#: app/fileItemMenu.js:376
#, fuzzy
msgid "Move to..."
msgstr "Siirtyminen..."

#: app/fileItemMenu.js:377
#, fuzzy
msgid "Copy to..."
msgstr "Kopioi..."

#: app/fileItemMenu.js:381
msgid "Rename…"
msgstr "Nimeä uudelleen…"

#: app/fileItemMenu.js:384
#, fuzzy
msgid "Create Link..."
msgstr "Luo linkki..."

#: app/fileItemMenu.js:389
#, fuzzy
msgid "Compress {0} folder"
msgid_plural "Compress {0} folders"
msgstr[0] "Komennus {0}"
msgstr[1] "Kompressoi kansiot"

#: app/fileItemMenu.js:396
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Pakkaa {0} tiedosto"
msgstr[1] "Pakkaa {0} tiedostoa"

#: app/fileItemMenu.js:402
#, fuzzy
msgid "Email to..."
msgstr "Sähköpostia..."

#: app/fileItemMenu.js:407
#, fuzzy
msgid "Send to Mobile Device"
msgstr "Lähetä mobiililaitteet"

#: app/fileItemMenu.js:411
msgid "Move to Trash"
msgstr "Siirrä roskakoriin"

#: app/fileItemMenu.js:414
msgid "Delete permanently"
msgstr "Poista pysyvästi"

#: app/fileItemMenu.js:419
msgid "Don't Allow Launching"
msgstr "Älä salli käynnistämistä"

#: app/fileItemMenu.js:419
msgid "Allow Launching"
msgstr "Salli käynnistäminen"

#: app/fileItemMenu.js:425
msgid "Empty Trash"
msgstr "Tyhjennä roskakori"

#: app/fileItemMenu.js:431
msgid "Eject"
msgstr "Irrota"

#: app/fileItemMenu.js:434
msgid "Unmount"
msgstr "Irrota osio"

#: app/fileItemMenu.js:438
msgid "Common Properties"
msgstr "Yhteiset ominaisuudet"

#: app/fileItemMenu.js:439
msgid "Properties"
msgstr "Ominaisuudet"

#: app/fileItemMenu.js:442
#, fuzzy
#| msgid "Show All in Files"
msgid "Show All in {0}"
msgstr "Näytä kaikki"

#: app/fileItemMenu.js:443
#, fuzzy
#| msgid "Show in Files"
msgid "Show in {0}"
msgstr "Näyttely {0}"

#: app/fileItemMenu.js:448
#, fuzzy
msgid "Open in {0}"
msgstr "Avoinna {0}"

#: app/fileItemMenu.js:567
#, fuzzy
msgid "Extraction Cancelled"
msgstr "Ylimääräinen peruttu"

#: app/fileItemMenu.js:568
#, fuzzy
msgid "Unable to extract File, no destination folder"
msgstr "Et voi ottaa File, Ei Kohteen kansiota"

#: app/fileItemMenu.js:600
#, fuzzy
msgid "Move Cancelled"
msgstr "Siirtyminen peruutettu"

#: app/fileItemMenu.js:601
#, fuzzy
msgid "Unable to move Files, no destination folder"
msgstr "Et voi siirtää tiedostoja, ei kohdekansiota"

#: app/fileItemMenu.js:613 app/fileItemMenu.js:642
#, fuzzy
msgid "Select Destination"
msgstr "Select Destination"

#: app/fileItemMenu.js:616 app/fileItemMenu.js:644
msgid "Select"
msgstr "Valitse"

#: app/fileItemMenu.js:693
#, fuzzy
msgid "Copy Cancelled"
msgstr "Kopiointi peruutettu"

#: app/fileItemMenu.js:694
#, fuzzy
msgid "Unable to copy Files, no destination folder"
msgstr "Et voi kopioida tiedostoja, ei kohdekansiota"

#: app/fileItemMenu.js:742 app/fileItemMenu.js:752 app/fileItemMenu.js:762
#: app/fileItemMenu.js:789
#, fuzzy
msgid "Mail Error"
msgstr "Sähkövirhe"

#: app/fileItemMenu.js:743
#, fuzzy
msgid "Unable to find xdg-email, please install the program"
msgstr "Et löydä xdg-sähköpostia, asenna ohjelma"

#: app/fileItemMenu.js:753
#, fuzzy
msgid "There was an error in emailing Files"
msgstr "Sähköpostiviesteissä oli virhe"

#: app/fileItemMenu.js:763
#, fuzzy
msgid "Unable to find zip command, please install the program"
msgstr "Et löydä zip-komentoa, asenna ohjelma"

#. Translators - basename for a zipped archive created for mailing
#: app/fileItemMenu.js:769
#, fuzzy
msgid "Archive.zip"
msgstr "Archive.zip"

#: app/fileItemMenu.js:790
#, fuzzy
msgid "There was an error in creating a zip archive"
msgstr "Epäonnistuneita zip-arkistossa"

#: app/fileItemMenu.js:803
msgid "Can not email a Directory"
msgstr "Kansiota ei voi lähettää sähköpostitse"

#: app/fileItemMenu.js:804
#, fuzzy
#| msgid ""
#| "Selection includes a Directory, compress the directory to a file first."
msgid "Selection includes a Directory, compress to a .zip file first?"
msgstr "Valinta sisältää hakemiston, pakkaa .zip-tiedostoon ensin?"

#: app/fileItemMenu.js:916
#, fuzzy
msgid "Unable to Open in Gnome Console"
msgstr "Et voi avata Gnome Consolessa"

#: app/fileItemMenu.js:917
#, fuzzy
msgid "Please Install Gnome Console or other Terminal Program"
msgstr "Asenna Gnome Console tai muu terminaali"

#: app/fileItemMenu.js:924
#, fuzzy
msgid "Unable to Open {0}"
msgstr "Ei voi avata {0}"

#: app/fileItemMenu.js:925
#, fuzzy
msgid "Please Install {0}"
msgstr "Ole hyvä ja asenna {0}"

#: app/preferences.js:417
#, fuzzy
msgid "Console"
msgstr "Konsoli"

#: app/showErrorPopup.js:39
#, fuzzy
msgid "More Information"
msgstr "Lisätietoja"

#. * TRANSLATORS: when using a screen reader, this is the text read when a stack is
#. selected. Example: if a stack named "pictures" is selected, it will say "Stack pictures"
#: app/stackItem.js:44
#, fuzzy
msgid "Stack"
msgstr "Pino"

#. eslint-disable-next-line no-template-curly-in-string
#: app/utils/dbusUtils.js:55
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr "\"${programName}\" tarvitaan työpöytäkuvakkeisiin"

#. eslint-disable-next-line no-template-curly-in-string
#: app/utils/dbusUtils.js:57
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""
"Tämä työpöytäkuvakkeiden toiminnallisuus vaatii ohjelman\"${programName}\" "
"asentamisen järjestelmään."

#: app/utils/desktopIconsUtil.js:183
msgid "Command not found"
msgstr "Komentoa ei löydy"

#: app/resources/ui/ding-app-chooser.ui:47
#, fuzzy
msgid "Choose an app to open the selected files."
msgstr "Valitse sovellus valittujen tiedostojen avaamiseksi."

#: app/resources/ui/ding-app-chooser.ui:81
#, fuzzy
msgid "Always use for this file type"
msgstr "Käytä aina tätä tiedostotyyppiä"

#: app/resources/ui/ding-app-chooser.ui:104
#, fuzzy
msgid "_Cancel"
msgstr "__Peruuta"

#: app/resources/ui/ding-app-chooser.ui:110
#, fuzzy
msgid "_Open"
msgstr "_Avaa"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:25
msgid "Icon size"
msgstr "Kuvakekoko"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "Aseta työpöytäkuvakkeiden koko."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:30
msgid "Show personal folder"
msgstr "Näytä kotikansio"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "Näytä kotikansio työpöydällä."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:35
msgid "Show trash icon"
msgstr "Näytä roskakorin kuvake"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "Näytä roskakorin kuvake työpöydällä."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:40
msgid "New icons start corner"
msgstr "Uusien kuvakkeiden aloituskulma"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "Aseta kulma jonne kuvakkeet sijoitetaan."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "Näytä erilliset tallennusvälineet työpöydällä"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "Näytä tietokoneeseen liitetyt tallennusvälineet."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "Näytä verkkoasemat työpöydällä"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "Näytä liitetyt verkkoasemat työpöydällä."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "Lisää uudet tallennusvälineet näytön toiselle puolelle"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Näytä lisätyt tallennusvälineet ja asemat työpöydän vastakkaisella puolella."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Näyttää suorakulmio kohteen kohdalla vetämisen ja pudottamisen aikana"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"Merkitsee kuvakkeen tulevan paikan ruudukossa osittain läpinäkyvällä "
"suorakulmiolla vetämisen ja pudottamisen aikana."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "Järjestä erikoishakemistot - Koti/Roskakori/Asemat."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"Kodin, Roskakorin ja liitettyjen verkko- ja ulkoisten asemien sijainnin "
"muuttamiseksi järjestäessä kuvakkeita työpöydällä"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr "Pidä kuvakkeet järjestettynä"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "Pidä kuvakkeet aina viimeksi valitun järjestyksen mukaisesti"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:75
msgid "Arrange Order"
msgstr "Kuvakkeiden järjestys"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr "Kuvakkeet järjestetään tämän ominaisuuden mukaan"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr "Pidä kuvakkeet pinottuina"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "Pidä aina kuvakkeet pinottuina, samantyyppiset ryhmissään"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr "Tiedostotyypit, joita ei pinota"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr "Taulukko merkkijonoja tiedostotyypeistä, joita ei pinota"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:90
msgid "Add an emblem to links"
msgstr "Merkitse tiedostolinkit symbolilla"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:91
msgid "Add an emblem to allow to identify soft links."
msgstr "Lisää symboli tiedostolinkkien tunnistamisen helpottamiseksi."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:95
msgid "Use black for label text"
msgstr "Käytä mustaa seliteteksteissä"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:96
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""
"Piirrä seliteteksti mustalla valkoisen sijaan. Hyödyllinen vaaleita "
"taustakuvia käyttäessä."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:100
#, fuzzy
msgid "Show new icons on non primary monitor if connected"
msgstr "Näytä uudet kuvakkeet ei-ensisijainen näyttö, jos kytketty"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:101
#, fuzzy
msgid ""
"If a second monitor is connected, new icons are placed on the non primary "
"monitor"
msgstr ""
"Jos toinen näyttö on kytketty toiseen näyttöön, uudet ikonit asetetaan ei-"
"ensisijaiseen näyttöön"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:105
#, fuzzy
msgid "Icons can be positioned anywhere on Desktop"
msgstr "Ikonit voidaan sijoittaa missä tahansa työpöydällä"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:106
#, fuzzy
msgid ""
"Icons are not on a rectangular grid but can be postioned anywhere "
"independent of grid"
msgstr ""
"Ikonit eivät ole suorakulmaisessa verkossa, mutta ne voidaan siirtää missä "
"tahansa verkkoverkosta riippumatta"

#~ msgid "Change Background…"
#~ msgstr "Vaihda taustakuvaa…"

#~ msgid "Display Settings"
#~ msgstr "Näytön asetukset"

#, fuzzy
#~ msgid "Rectangular icons with drop grid highlighting"
#~ msgstr "Rectangular icons with drop grid highlighting Näytä tarkat tiedot"

#~ msgid "Close"
#~ msgstr "Sulje"

#~ msgid "Use Nemo to open folders"
#~ msgstr "Käytä Nemoa kansioiden avaamiseen"

#~ msgid "Use Nemo instead of Nautilus to open folders."
#~ msgstr "Käytä Nemoa Nautiluksen sijaan kansioiden avaamiseen."

#, fuzzy
#~ msgid "Show Desktop In GNOME Files"
#~ msgstr "Näytä työpöytä GNOME-tiedostot"

#, fuzzy
#~ msgid "Open In Terminal"
#~ msgstr "Avaa päätteessä"

#~ msgid "Open in Terminal"
#~ msgstr "Avaa päätteessä"

#, fuzzy
#~ msgid "No Extraction Folder"
#~ msgstr "Ei purkukansiota"

#~ msgid "Select Extract Destination"
#~ msgstr "Valitse purkamiskohde"

#, fuzzy
#~ msgid "No Destination Folder"
#~ msgstr "Ei kohdekansiota"

#~ msgid "Open With Other Application"
#~ msgstr "Avaa toisella sovelluksella"

#~ msgid "Click type for open files"
#~ msgstr "Napsautustapa tiedostojen avaamiselle"

#~ msgid "Action to do when launching a program from the desktop"
#~ msgstr "Toiminta käynnistettäessä ohjelmaa työpöydältä"

#~ msgid "Display the content of the file"
#~ msgstr "Näytä tiedoston sisältö"

#~ msgid "Launch the file"
#~ msgstr "Käynnistä tiedosto"

#~ msgid "Ask what to do"
#~ msgstr "Kysy mitä tehdään"

#~ msgid "Local files only"
#~ msgstr "Vain paikalliset tiedostot"

#~ msgid ""
#~ "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
#~ msgstr ""
#~ "Nautilus-tiedostonhallinta vaaditaan tämän laajennuksen käyttämiseen."

#~ msgid "Sort by Name"
#~ msgstr "Järjestä nimen mukaan"

#~ msgid "Sort by Name Descending"
#~ msgstr "Järjestä nimen mukaan laskevasti"

#~ msgid "Sort by Type"
#~ msgstr "Järjestä tyypin mukaan"

#~ msgid "Sort by Size"
#~ msgstr "Järjestä koon mukaan"

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Muuttaaksesi työpöytäkuvakkeiden asetuksia, napsauta työpöytää hiiren "
#~ "oikealla painikkeella ja valitse viimeinen kohde ”Työpöytäkuvakkeiden "
#~ "asetukset”"

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "Haluatko suorittaa tiedoston “{0}” vai näyttää sen sisällön?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "“{0}” on suoritettava oleva tekstitiedosto."

#~ msgid "Execute in a terminal"
#~ msgstr "Suorita päätteessä"

#~ msgid "Show"
#~ msgstr "Näytä"

#~ msgid "Execute"
#~ msgstr "Suorita"

#~ msgid "New folder"
#~ msgstr "Uusi kansio"

#~ msgid "Delete"
#~ msgstr "Poista"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "Haluatko varmasti poistaa nämä kohteet pysyvästi?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "Jos poistat kohteen, se häviää pysyvästi."

#~ msgid "Desktop Icons Settings"
#~ msgstr "Työpöytäkuvakkeiden asetukset"

#~ msgid "Arrange By..."
#~ msgstr "Järjestä..."

#~ msgid "Keep Arranged..."
#~ msgstr "Pidä järjestettynä..."

#~ msgid "Keep Stacked by type..."
#~ msgstr "Pidä pinottuna tyypin mukaan..."

#~ msgid "Sort Home/Drives/Trash..."
#~ msgstr "Järjestä koti/asemat/roskakori..."

#~ msgid "Run as a program"
#~ msgstr "Suorita ohjelmana"

#~ msgid "Error while deleting files"
#~ msgstr "Virhe poistettaessa tiedostoja"

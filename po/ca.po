# This file is distributed under the same license as the PACKAGE package.
# Jordi Mas i Hernandez <jmas@softcatala.org>, 2019-2021
# Weblate Translation Memory <noreply-mt-weblate-translation-memory@weblate.org>, 2023, 2024.
# Weblate <noreply-mt-weblate@weblate.org>, 2023, 2024.
# LibreTranslate <noreply-mt-libretranslate@weblate.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: gtk4-ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-07-04 10:36-0400\n"
"PO-Revision-Date: 2024-09-02 19:09+0000\n"
"Last-Translator: Weblate Translation Memory <noreply-mt-weblate-translation-"
"memory@weblate.org>\n"
"Language-Team: Catalan <https://hosted.weblate.org/projects/"
"gtk4-desktop-icons-ng/gtk4-ding-pot/ca/>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.8-dev\n"

#: app/adwPreferencesWindow.js:175 app/adwPreferencesWindow.js:176
#, fuzzy
msgid "Desktop"
msgstr "Escriptori"

#: app/adwPreferencesWindow.js:180 app/adwPreferencesWindow.js:181
#, fuzzy
msgid "Files"
msgstr "Fitxers"

#: app/adwPreferencesWindow.js:185 app/adwPreferencesWindow.js:186
#: app/adwPreferencesWindow.js:216
#, fuzzy
msgid "Tweaks"
msgstr "Retocs"

#: app/adwPreferencesWindow.js:190 app/adwPreferencesWindow.js:191
#, fuzzy
msgid "About"
msgstr "Quant a"

#: app/adwPreferencesWindow.js:201
#, fuzzy
msgid "Desktop Settings"
msgstr "Configuració d'escriptori"

#: app/adwPreferencesWindow.js:202
#, fuzzy
msgid "Settings for the Desktop Program"
msgstr "Configuració del programa d'escriptori"

#: app/adwPreferencesWindow.js:206
#, fuzzy
msgid "Volumes"
msgstr "Volums"

#: app/adwPreferencesWindow.js:207
#, fuzzy
msgid "Desktop volumes display"
msgstr "Expositor de volums d'escriptori"

#: app/adwPreferencesWindow.js:211
#, fuzzy
msgid "Files Settings"
msgstr "Configuració de fitxers"

#: app/adwPreferencesWindow.js:212
#, fuzzy
msgid "Settings shared with Gnome Files"
msgstr "Configuració compartida amb Gnome Files"

#: app/adwPreferencesWindow.js:217
#, fuzzy
msgid "Miscellaneous Tweaks"
msgstr "Miscel·lània ajustaments"

#: app/adwPreferencesWindow.js:228
msgid "Size for the desktop icons"
msgstr "Mida de les icones d'escriptori"

#: app/adwPreferencesWindow.js:230
msgid "Tiny"
msgstr "Diminuta"

#: app/adwPreferencesWindow.js:231
msgid "Small"
msgstr "Petita"

#: app/adwPreferencesWindow.js:232
msgid "Standard"
msgstr "Estàndard"

#: app/adwPreferencesWindow.js:233
msgid "Large"
msgstr "Gran"

#: app/adwPreferencesWindow.js:238
msgid "New icons alignment"
msgstr "Alineació de les icones noves"

#: app/adwPreferencesWindow.js:240
#, fuzzy
msgid "Top left corner"
msgstr "Cantonada superior esquerra"

#: app/adwPreferencesWindow.js:241
#, fuzzy
msgid "Top right corner"
msgstr "Cantonada superior dreta"

#: app/adwPreferencesWindow.js:242
#, fuzzy
msgid "Bottom left corner"
msgstr "Cantonada inferior esquerra"

#: app/adwPreferencesWindow.js:243
#, fuzzy
msgid "Bottom right corner"
msgstr "Cantonada inferior dreta"

#: app/adwPreferencesWindow.js:248
#, fuzzy
msgid "Add new icons to Secondary Monitors first, if available"
msgstr ""
"Afegir noves icones als monitors de secundària primer, si està disponible"

#: app/adwPreferencesWindow.js:252
#, fuzzy
msgid "Show the personal folder on the desktop"
msgstr "Mostra la carpeta personal a l'escriptori"

#: app/adwPreferencesWindow.js:256
#, fuzzy
msgid "Show the trash icon on the desktop"
msgstr "Mostra la icona de la paperera a l'escriptori"

#: app/adwPreferencesWindow.js:260
#, fuzzy
msgid "Show external drives on the desktop"
msgstr "Mostra les unitats externes a l'escriptori"

#: app/adwPreferencesWindow.js:264
#, fuzzy
msgid "Show network drives on the desktop"
msgstr "Mostra les unitats de xarxa a l'escriptori"

#: app/adwPreferencesWindow.js:268
#, fuzzy
msgid "Add new drives to the opposite side of the desktop"
msgstr "Afegeix noves unitats al costat oposat de la pantalla"

#: app/adwPreferencesWindow.js:273
#, fuzzy
msgid "Snap icons to grid"
msgstr "Petar icones a la graella"

#: app/adwPreferencesWindow.js:278
#, fuzzy
msgid "Highlight the drop grid"
msgstr "Destaca la reixeta de gota"

#: app/adwPreferencesWindow.js:285
#, fuzzy
msgid "Add an emblem to soft links"
msgstr "Afegir un emblema als enllaços suaus"

#: app/adwPreferencesWindow.js:288
#, fuzzy
msgid "Use dark text in icon labels"
msgstr "Utilitzar el text fosc en les etiquetes de les icones"

#: app/adwPreferencesWindow.js:293
#, fuzzy
msgid "Action to Open Items"
msgstr "Acció per a Open Items"

#: app/adwPreferencesWindow.js:295
msgid "Single click"
msgstr "Un sol clic"

#: app/adwPreferencesWindow.js:296
msgid "Double click"
msgstr "Doble clic"

#: app/adwPreferencesWindow.js:300
msgid "Show image thumbnails"
msgstr "Mostra les miniatures de les imatges"

#: app/adwPreferencesWindow.js:302
msgid "Always"
msgstr "Sempre"

#: app/adwPreferencesWindow.js:303
#, fuzzy
msgid "On this computer only"
msgstr "Només en aquest ordinador"

#: app/adwPreferencesWindow.js:304
msgid "Never"
msgstr "Mai"

#: app/adwPreferencesWindow.js:308
msgid "Show a context menu item to delete permanently"
msgstr "Mostra un element de menú contextual per a suprimir-lo permanentment"

#: app/adwPreferencesWindow.js:312
msgid "Show hidden files"
msgstr "Mostra els fitxers ocults"

#: app/adwPreferencesWindow.js:316
#, fuzzy
msgid "Open folders on drag hover"
msgstr "Obriu les carpetes en l'horitzonta d'arrossegament"

#: app/adwPreferencesWindow.js:319
#, fuzzy
msgid "Website"
msgstr "Pàgina web"

#: app/adwPreferencesWindow.js:321
#, fuzzy
msgid "Visit"
msgstr "Visita"

#: app/adwPreferencesWindow.js:324
#, fuzzy
msgid "Issues"
msgstr "Problemes"

#: app/adwPreferencesWindow.js:325
#, fuzzy
msgid "Report issues on issue tracker"
msgstr "Informes sobre el seguiment d'incidències"

#: app/adwPreferencesWindow.js:326
#, fuzzy
msgid "Report"
msgstr "Informe"

#: app/adwPreferencesWindow.js:328
#, fuzzy
msgid "License"
msgstr "Llicència"

#: app/adwPreferencesWindow.js:333
#, fuzzy
msgid "Translation"
msgstr "Traducció"

#: app/adwPreferencesWindow.js:334
#, fuzzy
msgid "Help translate in your web browser"
msgstr "Ajuda a traduir el teu navegador web"

#: app/adwPreferencesWindow.js:335
#, fuzzy
msgid "Translate"
msgstr "Traducció"

#: app/appChooser.js:54
#, fuzzy
msgid "Choose an application to open <b>{foo}</b>"
msgstr "<b>{foo}</b>"

#: app/appChooser.js:62
#, fuzzy
msgid "Open Items"
msgstr "Abrir Elemento"

#: app/appChooser.js:64
#, fuzzy
msgid "Open Folder"
msgstr "Obre la carpeta"

#: app/appChooser.js:66 app/resources/ui/ding-app-chooser.ui:6
#, fuzzy
msgid "Open File"
msgstr "Obre un fitxer"

#: app/appChooser.js:106
#, fuzzy
msgid "Error changing default application"
msgstr "Preguntes Freqüents - FAQ"

#: app/appChooser.js:107
#, fuzzy
msgid "Error while setting {foo} as default application for {mimetype}"
msgstr "{font} per {font} per {font} per {font}"

#: app/askRenamePopup.js:49
msgid "Folder name"
msgstr "Nom de la carpeta"

#: app/askRenamePopup.js:49
msgid "File name"
msgstr "Nom del fitxer"

#: app/askRenamePopup.js:57 app/autoAr.js:299 app/desktopManager.js:1243
#: app/fileItemMenu.js:805
msgid "OK"
msgstr "D'acord"

#: app/askRenamePopup.js:57
msgid "Rename"
msgstr "Canvia el nom"

#: app/autoAr.js:71
#, fuzzy
msgid "AutoAr is not installed"
msgstr "Acte Ar no està instal·lat"

#: app/autoAr.js:72
#, fuzzy
msgid ""
"To be able to work with compressed files, install file-roller and/or gir-1.2-"
"gnomeAutoAr"
msgstr ""
"Per poder treballar amb fitxers comprimits, instal·lar file-roller i/o "
"gir-1.2-gnome AutoAr"

#: app/autoAr.js:203
#, fuzzy
msgid "Extracting files"
msgstr "Extracció de fitxers"

#: app/autoAr.js:220
#, fuzzy
msgid "Compressing files"
msgstr "Comprimint arxius."

#: app/autoAr.js:291 app/autoAr.js:620 app/desktopManager.js:773
#: app/desktopManager.js:1245 app/fileItemMenu.js:651 app/fileItemMenu.js:805
#: app/showErrorPopup.js:39 app/showErrorPopup.js:43
msgid "Cancel"
msgstr "Cancel·la"

#: app/autoAr.js:311 app/autoAr.js:608
#, fuzzy
msgid "Enter a password here"
msgstr "Introduïu la contrasenya aquí"

#: app/autoAr.js:354
#, fuzzy
msgid "Removing partial file '${outputFile}'"
msgstr "Eliminació de fitxers parcials '${outputFile} '"

#: app/autoAr.js:373
#, fuzzy
msgid "Creating destination folder"
msgstr "Creació de carpeta de destinació"

#: app/autoAr.js:405
#, fuzzy
msgid "Extracting files into '${outputPath}'"
msgstr "Extracció de fitxers a '${outputPath} '"

#: app/autoAr.js:435
#, fuzzy
msgid "Extraction completed"
msgstr "Extracció realitzada"

#: app/autoAr.js:436
#, fuzzy
msgid "Extracting '${fullPathFile}' has been completed."
msgstr "S'ha completat l'extracció de ${fullPathFile}."

#: app/autoAr.js:442
#, fuzzy
msgid "Extraction cancelled"
msgstr "Extracció cancel·lada"

#: app/autoAr.js:443
#, fuzzy
msgid "Extracting '${fullPathFile}' has been cancelled by the user."
msgstr "L'extracció de '${fullPathFile}' ha estat anul·lada per l'usuari."

#: app/autoAr.js:453
#, fuzzy
msgid "Passphrase required for ${filename}"
msgstr "Per a ${filename}"

#: app/autoAr.js:456
#, fuzzy
msgid "Error during extraction"
msgstr "Error en l'extracció"

#: app/autoAr.js:483
#, fuzzy
msgid "Compressing files into '${outputFile}'"
msgstr "Compressió de fitxers a '${outputFile} '"

#: app/autoAr.js:496
#, fuzzy
msgid "Compression completed"
msgstr "Compressió completada"

#: app/autoAr.js:497
#, fuzzy
msgid "Compressing files into '${outputFile}' has been completed."
msgstr "S'ha completat la compressió de fitxers a '${outputFile}'."

#: app/autoAr.js:501 app/autoAr.js:508
#, fuzzy
msgid "Cancelled compression"
msgstr "Compressió anul·lada"

#: app/autoAr.js:502
#, fuzzy
msgid "The output file '${outputFile}' already exists."
msgstr "El fitxer de sortida '${outputFile}' ja existeix."

#: app/autoAr.js:509
#, fuzzy
msgid "Compressing files into '${outputFile}' has been cancelled by the user."
msgstr "S'han cancel·lat els fitxers de compressió a '${outputFile}'."

#: app/autoAr.js:512
#, fuzzy
msgid "Error during compression"
msgstr "Error durant la compressió"

#: app/autoAr.js:547
#, fuzzy
msgid "Create archive"
msgstr "Creant fitxer comprimit"

#: app/autoAr.js:571
#, fuzzy
msgid "Archive name"
msgstr "Nom de l'arxiu"

#: app/autoAr.js:604
#, fuzzy
msgid "Password"
msgstr "Contrasenya"

#: app/autoAr.js:617
#, fuzzy
msgid "Create"
msgstr "Crea"

#: app/autoAr.js:688
#, fuzzy
msgid "Compatible with all operating systems."
msgstr "Compatible amb tots els sistemes operatius."

#: app/autoAr.js:694
#, fuzzy
msgid "Password protected .zip, must be installed on Windows and Mac."
msgstr "Preguntes Freqüents - FAQ."

#: app/autoAr.js:700
#, fuzzy
msgid "Smaller archives but Linux and Mac only."
msgstr "Més petits arxius però només Linux i Mac."

#: app/autoAr.js:706
#, fuzzy
msgid "Smaller archives but must be installed on Windows and Mac."
msgstr "Els arxius més petits, però s'han d'instal·lar a Windows i Mac."

#: app/desktopGrid.js:47
#, fuzzy
msgid "Desktop Icons"
msgstr "Icona de l'escriptori"

#: app/desktopManager.js:139
#, fuzzy
msgid "GNOME Files not found"
msgstr "GNOME Arxius no trobats"

#: app/desktopManager.js:140
#, fuzzy
msgid "The GNOME Files application is required by Gtk4 Desktop Icons NG."
msgstr "El GNOME L'aplicació d'arxius és necessària per Gtk4 Desktop Icons NG."

#: app/desktopManager.js:152
#, fuzzy
msgid "There is no default File Manager"
msgstr "No hi ha cap gestor de fitxers predeterminat"

#: app/desktopManager.js:153
#, fuzzy
msgid "There is no application that handles mimetype \"inode/directory\""
msgstr "No hi ha aplicació que maneja mimetype \"inode/directory\""

#: app/desktopManager.js:165
#, fuzzy
msgid "Gnome Files is not registered as a File Manager"
msgstr "Gnome Files no està registrat com a gestor de fitxers"

#: app/desktopManager.js:166
#, fuzzy
msgid ""
"The Gnome Files application is not programmed to open Folders!\n"
"Check your xdg-utils installation\n"
"Check Gnome Files .desktop File installation"
msgstr ""
"L’aplicació Gnome Files no està programada per obrir carpetes!\n"
"Consulta la teva instal·lació\n"
"Preguntes Freqüents - FAQ Instal·lació de fitxers"

#: app/desktopManager.js:772
#, fuzzy
msgid "Choose Action for Files"
msgstr "Trieu l'acció dels fitxers"

#: app/desktopManager.js:773
#, fuzzy
msgid "Move"
msgstr "Mou"

#: app/desktopManager.js:773 app/fileItemMenu.js:372
msgid "Copy"
msgstr "Copia"

#: app/desktopManager.js:773
#, fuzzy
msgid "Link"
msgstr "Vincula"

#: app/desktopManager.js:860 app/desktopManager.js:892
#, fuzzy
msgid "Making SymLink Failed"
msgstr "Making SymLink Failed"

#: app/desktopManager.js:861 app/desktopManager.js:893
#, fuzzy
msgid "Could not create symbolic link"
msgstr "No es pot crear un enllaç simbòlic"

#: app/desktopManager.js:1203
#, fuzzy
msgid "Clear current selection before new search"
msgstr "Esborrar la selecció actual abans de la nova cerca"

#: app/desktopManager.js:1247
#, fuzzy
msgid "Find Files on Desktop"
msgstr "Trobar fitxers a l'escriptori"

#: app/desktopManager.js:1510
#, fuzzy
msgid "Name"
msgstr "Nom"

#: app/desktopManager.js:1511
#, fuzzy
msgid "Name Z-A"
msgstr "Nom (Z-A)"

#: app/desktopManager.js:1512
#, fuzzy
msgid "Modified Time"
msgstr "Temps Modificat"

#: app/desktopManager.js:1513
#, fuzzy
msgid "Type"
msgstr "Tipus"

#: app/desktopManager.js:1514
#, fuzzy
msgid "Size"
msgstr "Mida"

#: app/desktopManager.js:1517
#, fuzzy
msgid "Keep Arranged…"
msgstr "Segueix organitzat.."

#: app/desktopManager.js:1521
#, fuzzy
msgid "Keep Stacked by Type…"
msgstr "Segueix apilat per Tipus.."

#: app/desktopManager.js:1522
#, fuzzy
msgid "Sort Home/Drives/Trash…"
msgstr "Sort Home/Drives/Trash.."

#: app/desktopManager.js:1527 app/desktopManager.js:2578
msgid "New Folder"
msgstr "Carpeta nova"

#: app/desktopManager.js:1531
msgid "New Document"
msgstr "Document nou"

#: app/desktopManager.js:1535
msgid "Paste"
msgstr "Enganxa"

#: app/desktopManager.js:1536
msgid "Undo"
msgstr "Desfés"

#: app/desktopManager.js:1537
msgid "Redo"
msgstr "Refés"

#: app/desktopManager.js:1542
#, fuzzy
msgid "Select All"
msgstr "Selecciona-ho tot"

#: app/desktopManager.js:1547
#, fuzzy
msgid "Arrange Icons"
msgstr "Organitzar Icones"

#: app/desktopManager.js:1551
#, fuzzy
msgid "Arrange By…"
msgstr "Organitzar Per.."

#: app/desktopManager.js:1557
#, fuzzy
#| msgid "Show Desktop in Files"
msgid "Show Desktop In {0}"
msgstr "Mostra l'escriptori a {0}"

#: app/desktopManager.js:1560
#, fuzzy
msgid "Open In {0}"
msgstr "Obrir {0}"

#: app/desktopManager.js:1566
#, fuzzy
msgid "Desktop Icon Settings"
msgstr "Configuració d'icones d'escriptori"

#: app/desktopManager.js:1571
msgid "Shell Menu…"
msgstr ""

#: app/desktopManager.js:1617
#, fuzzy
msgid "Preferences Window is Open"
msgstr "Preferències Finestra oberta"

#: app/desktopManager.js:1617
#, fuzzy
msgid "This Window is open. Please switch to the active window."
msgstr "Aquesta finestra està oberta. Canvieu la finestra activa."

#: app/desktopManager.js:1631
msgid "Settings"
msgstr "Paràmetres"

#: app/desktopManager.js:2601
#, fuzzy
msgid "Folder Creation Failed"
msgstr "Creació de carpetes"

#: app/desktopManager.js:2602
#, fuzzy
msgid "Could not create folder"
msgstr "Error en crear la carpeta"

#: app/desktopManager.js:2645
#, fuzzy
msgid "Template Creation Error"
msgstr "Error de creació de plantilla"

#: app/desktopManager.js:2646
#, fuzzy
msgid "Could not create document"
msgstr "No s'ha pogut suprimir el document"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: app/fileItem.js:138 app/fileItem.js:147
msgid "Home"
msgstr "Inici"

#: app/fileItem.js:153
#, fuzzy
#| msgid "Empty Trash"
msgid "Trash"
msgstr "Cistella"

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * an external drive is selected. Example: if a USB stick named "my_portable"
#. * is selected, it will say "Drive my_portable"
#: app/fileItem.js:159
#, fuzzy
msgid "Drive"
msgstr "Disc"

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * a folder is selected. Example: if a folder named "things" is selected,
#. * it will say "Folder things"
#: app/fileItem.js:169
#, fuzzy
#| msgid "New Folder"
msgid "Folder"
msgstr "Carpeta"

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * a normal file is selected. Example: if a file named "my_picture.jpg"
#. * is selected, it will say "File my_picture.jpg"
#: app/fileItem.js:176
#, fuzzy
msgid "File"
msgstr "Fitxer"

#: app/fileItem.js:305
#, fuzzy
msgid "Broken Link"
msgstr "enllaç no vàlid"

#: app/fileItem.js:306
#, fuzzy
msgid "Can not open this File because it is a Broken Symlink"
msgstr "No es pot obrir aquest fitxer perquè és un enllaç trencat"

#: app/fileItem.js:337
#, fuzzy
msgid "Opening File Failed"
msgstr "Obrir fitxer fallat"

#: app/fileItem.js:339
#, fuzzy
msgid "There is no application installed to open \"{foo}\" files."
msgstr "No s'ha instal·lat cap aplicació per obrir fitxers \"{foo}\"."

#: app/fileItem.js:368
#, fuzzy
msgid "Broken Desktop File"
msgstr "Fitxers .desktop trencats"

#: app/fileItem.js:369
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"Edit the file to set the correct executable Program."
msgstr ""

#: app/fileItem.js:375
#, fuzzy
msgid "Invalid Permissions on Desktop File"
msgstr "Preguntes Freqüents - FAQ"

#: app/fileItem.js:376
#, fuzzy
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""
"Aquest .desktop El fitxer té permisos incorrectes. Feu clic dret per editar "
"propietats, llavors:\n"

#: app/fileItem.js:378
#, fuzzy
msgid ""
"\n"
"Set Permissions, in \"Others Access\", \"Read Only\" or \"None\""
msgstr ""
"\n"
"Set Permissions, a \"Others Access\", \"Read Only\" o \"None\""

#: app/fileItem.js:381
#, fuzzy
msgid ""
"\n"
"Enable option, \"Allow Executing File as a Program\""
msgstr ""
"\n"
"Activa l'opció \"Permet executar fitxers com a programa\""

#: app/fileItem.js:388
#, fuzzy
msgid "Untrusted Desktop File"
msgstr "Fitxer d'escriptori no confiat"

#: app/fileItem.js:389
#, fuzzy
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"Enable \"Allow Launching\""
msgstr ""
"Això .fitxer d'escriptori no és confiat, no pot ser llançat. Per habilitar "
"llançar, bé-clic, llavors:\n"
"\n"
"<b>Habilitar \"Permetre Llançar\"</b>"

#: app/fileItem.js:399 app/gnomeShellDragDrop.js:238
#, fuzzy
msgid "Could not open File"
msgstr "No s'ha pogut obrir el fitxer"

#. eslint-disable-next-line no-template-curly-in-string
#: app/fileItem.js:401 app/gnomeShellDragDrop.js:240
#, fuzzy
msgid "${appName} can not open files of this Type!"
msgstr "${appName} no pot obrir fitxers d'aquest tipus!"

#: app/fileItemMenu.js:298
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Carpeta nova amb {0} element"
msgstr[1] "Carpeta nova amb {0} elements"

#: app/fileItemMenu.js:305
msgid "Open All..."
msgstr "Obre-ho tot..."

#: app/fileItemMenu.js:316
#, fuzzy
msgid "Run"
msgstr "Executa"

#: app/fileItemMenu.js:318
#, fuzzy
msgid "Open with {foo}"
msgstr "Obert amb {foo}"

#: app/fileItemMenu.js:320
msgid "Open"
msgstr "Obre"

#: app/fileItemMenu.js:329 app/fileItemMenu.js:333
msgid "Extract Here"
msgstr "Extreu aquí"

#: app/fileItemMenu.js:335
msgid "Extract To..."
msgstr "Extreu a..."

#: app/fileItemMenu.js:340 app/fileItemMenu.js:343
#, fuzzy
msgid "Open With..."
msgstr "Obre amb..."

#: app/fileItemMenu.js:343
msgid "Open All With Other Application..."
msgstr "Obre amb una altra aplicació..."

#: app/fileItemMenu.js:346
msgid "Launch using Dedicated Graphics Card"
msgstr "Llança usant targeta gràfica dedicada"

#: app/fileItemMenu.js:353
#, fuzzy
msgid "Stack This Type"
msgstr "Stack Aquest tipus"

#: app/fileItemMenu.js:353
#, fuzzy
msgid "Unstack This Type"
msgstr "Desapilar aquest tipus"

#: app/fileItemMenu.js:364
#, fuzzy
msgid "Run as a Program"
msgstr "Executar com a programa"

#: app/fileItemMenu.js:367
msgid "Scripts"
msgstr "Scripts"

#: app/fileItemMenu.js:370
msgid "Cut"
msgstr "Retalla"

#: app/fileItemMenu.js:376
#, fuzzy
msgid "Move to..."
msgstr "Mou a ..."

#: app/fileItemMenu.js:377
#, fuzzy
msgid "Copy to..."
msgstr "Copia a ..."

#: app/fileItemMenu.js:381
msgid "Rename…"
msgstr "Canvia el nom…"

#: app/fileItemMenu.js:384
#, fuzzy
msgid "Create Link..."
msgstr "Crear enllaç..."

#: app/fileItemMenu.js:389
#, fuzzy
msgid "Compress {0} folder"
msgid_plural "Compress {0} folders"
msgstr[0] "Compres {0} carpeta"
msgstr[1] "Compress {0} carpetes"

#: app/fileItemMenu.js:396
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Comprimeix {0} fitxer"
msgstr[1] "Comprimeix {0} fitxers"

#: app/fileItemMenu.js:402
#, fuzzy
msgid "Email to..."
msgstr "Correu electrònic."

#: app/fileItemMenu.js:407
#, fuzzy
msgid "Send to Mobile Device"
msgstr "Enviar al dispositiu mòbil"

#: app/fileItemMenu.js:411
msgid "Move to Trash"
msgstr "Mou a la paperera"

#: app/fileItemMenu.js:414
msgid "Delete permanently"
msgstr "Suprimeix permanentment"

#: app/fileItemMenu.js:419
msgid "Don't Allow Launching"
msgstr "No permetis que s'iniciï"

#: app/fileItemMenu.js:419
msgid "Allow Launching"
msgstr "Permet que s'iniciï"

#: app/fileItemMenu.js:425
msgid "Empty Trash"
msgstr "Buida la paperera"

#: app/fileItemMenu.js:431
msgid "Eject"
msgstr "Expulsa"

#: app/fileItemMenu.js:434
msgid "Unmount"
msgstr "Desmunta"

#: app/fileItemMenu.js:438
msgid "Common Properties"
msgstr "Propietats comunes"

#: app/fileItemMenu.js:439
msgid "Properties"
msgstr "Propietats"

#: app/fileItemMenu.js:442
#, fuzzy
#| msgid "Show All in Files"
msgid "Show All in {0}"
msgstr "Mostrar tot en {0}"

#: app/fileItemMenu.js:443
#, fuzzy
#| msgid "Show in Files"
msgid "Show in {0}"
msgstr "Mostrar a {0}"

#: app/fileItemMenu.js:448
#, fuzzy
msgid "Open in {0}"
msgstr "Obert a {0}"

#: app/fileItemMenu.js:567
#, fuzzy
msgid "Extraction Cancelled"
msgstr "Extracció Cancel·lat"

#: app/fileItemMenu.js:568
#, fuzzy
msgid "Unable to extract File, no destination folder"
msgstr "No es pot extreure Arxiu, cap carpeta de destinació"

#: app/fileItemMenu.js:600
#, fuzzy
msgid "Move Cancelled"
msgstr "Move Cancel·lat"

#: app/fileItemMenu.js:601
#, fuzzy
msgid "Unable to move Files, no destination folder"
msgstr "No es pot moure fitxers, cap carpeta de destinació"

#: app/fileItemMenu.js:613 app/fileItemMenu.js:642
#, fuzzy
msgid "Select Destination"
msgstr "Selecciona un destí"

#: app/fileItemMenu.js:616 app/fileItemMenu.js:644
#, fuzzy
msgid "Select"
msgstr "Selecciona"

#: app/fileItemMenu.js:693
#, fuzzy
msgid "Copy Cancelled"
msgstr "Cancel·lat"

#: app/fileItemMenu.js:694
#, fuzzy
msgid "Unable to copy Files, no destination folder"
msgstr "No es pot copiar fitxers, cap carpeta de destinació"

#: app/fileItemMenu.js:742 app/fileItemMenu.js:752 app/fileItemMenu.js:762
#: app/fileItemMenu.js:789
#, fuzzy
msgid "Mail Error"
msgstr "Correu electrònic"

#: app/fileItemMenu.js:743
#, fuzzy
msgid "Unable to find xdg-email, please install the program"
msgstr ""
"Si us plau, introdueix dins de la caixa de text els caràcters que veu a la "
"imatge de sota. Això és requerit per evitar enviaments automàtics"

#: app/fileItemMenu.js:753
#, fuzzy
msgid "There was an error in emailing Files"
msgstr "Hi ha un error en el correu electrònic"

#: app/fileItemMenu.js:763
#, fuzzy
msgid "Unable to find zip command, please install the program"
msgstr "Incapaç de trobar l'ordre zip, instal·leu el programa"

#. Translators - basename for a zipped archive created for mailing
#: app/fileItemMenu.js:769
#, fuzzy
msgid "Archive.zip"
msgstr "Arxiu.zip"

#: app/fileItemMenu.js:790
#, fuzzy
msgid "There was an error in creating a zip archive"
msgstr "Hi ha un error en la creació d'un arxiu"

#: app/fileItemMenu.js:803
msgid "Can not email a Directory"
msgstr "No es pot enviar un directori per correu electrònic"

#: app/fileItemMenu.js:804
#, fuzzy
#| msgid ""
#| "Selection includes a Directory, compress the directory to a file first."
msgid "Selection includes a Directory, compress to a .zip file first?"
msgstr "La selecció inclou un directori, comprimeix a un fitxer .zip primer?"

#: app/fileItemMenu.js:916
#, fuzzy
msgid "Unable to Open in Gnome Console"
msgstr "No es pot obrir a la consola Gnome"

#: app/fileItemMenu.js:917
#, fuzzy
msgid "Please Install Gnome Console or other Terminal Program"
msgstr ""
"Si us plau, instal·leu la consola Gnome o un altre programa de terminals"

#: app/fileItemMenu.js:924
#, fuzzy
msgid "Unable to Open {0}"
msgstr "Obrir {0}"

#: app/fileItemMenu.js:925
#, fuzzy
msgid "Please Install {0}"
msgstr "Si us plau, instal·leu {0}"

#: app/preferences.js:417
#, fuzzy
msgid "Console"
msgstr "Consola"

#: app/showErrorPopup.js:39
#, fuzzy
msgid "More Information"
msgstr "Més informació"

#. * TRANSLATORS: when using a screen reader, this is the text read when a stack is
#. selected. Example: if a stack named "pictures" is selected, it will say "Stack pictures"
#: app/stackItem.js:44
#, fuzzy
msgid "Stack"
msgstr "Pila"

#. eslint-disable-next-line no-template-curly-in-string
#: app/utils/dbusUtils.js:55
#, fuzzy
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr "\"${programName}\" és necessari per a Icones d'escriptori"

#. eslint-disable-next-line no-template-curly-in-string
#: app/utils/dbusUtils.js:57
#, fuzzy
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""
"Per a aquesta funcionalitat per treballar en Icones d'escriptori, heu "
"d'instal·lar \"${programName}\" al vostre sistema."

#: app/utils/desktopIconsUtil.js:183
msgid "Command not found"
msgstr "No s'ha trobat l'ordre"

#: app/resources/ui/ding-app-chooser.ui:47
#, fuzzy
msgid "Choose an app to open the selected files."
msgstr "Trieu una aplicació per obrir els fitxers seleccionats."

#: app/resources/ui/ding-app-chooser.ui:81
#, fuzzy
msgid "Always use for this file type"
msgstr "Utilitzeu sempre aquest tipus de fitxer"

#: app/resources/ui/ding-app-chooser.ui:104
#, fuzzy
msgid "_Cancel"
msgstr "_Cancel·la"

#: app/resources/ui/ding-app-chooser.ui:110
#, fuzzy
msgid "_Open"
msgstr "_Obrir"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:25
msgid "Icon size"
msgstr "Mida d'icona"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "Estableix la mida per les icones de l'escriptori."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:30
msgid "Show personal folder"
msgstr "Mostra la carpeta personal"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "Mostra la carpeta personal a l'escriptori."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:35
msgid "Show trash icon"
msgstr "Mostra la icona de la paperera"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "Mostra la icona de la paperera a l'escriptori."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:40
msgid "New icons start corner"
msgstr "Angle d'inici de les icones noves"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "Estableix la cantonada des d'on s'iniciaran les icones."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "Mostra les unitats externes a l'escriptori"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "Mostra les unitats de disc connectades a l'ordinador."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "Mostra les unitats de xarxa a l'escriptori"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "Mostra els volums de xarxa muntats a l'escriptori."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "Afegeix noves unitats al costat oposat de la pantalla"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Quan s'afegeixin unitats i volums a l'escriptori, afegiu-los a la part "
"oposada de la pantalla."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Mostra un rectangle al lloc de destinació durant el DnD"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"En fer una operació d'arrossegar i deixar anar, marca el lloc a la graella "
"on la icona es posarà amb un rectangle semitransparent."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:65
#, fuzzy
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "Preguntes Freqüents - FAQ."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:66
#, fuzzy
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"Quan l'organització Icons a l'escriptori, ordenar i canviar la posició de la "
"llar, escombraries i muntat xarxa o unitats externes"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:70
#, fuzzy
msgid "Keep Icons Arranged"
msgstr "Mantenir Icones Organitzades"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:71
#, fuzzy
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "Manteniu sempre Icones Organitzada per l'última ordre concertada"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:75
#, fuzzy
msgid "Arrange Order"
msgstr "Ordenar"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:76
#, fuzzy
msgid "Icons Arranged by this property"
msgstr "Icones ordenades per aquesta propietat"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:80
#, fuzzy
msgid "Keep Icons Stacked"
msgstr "Mantingui les icones apilades"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:81
#, fuzzy
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "Manteniu sempre les icones apilades, s'agrupen tipus similars"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:85
#, fuzzy
msgid "Type of Files to not Stack"
msgstr "Tipus d'arxius a no pila"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:86
#, fuzzy
msgid "An Array of strings types, Don't Stack these types of files"
msgstr "No apilar aquest tipus de fitxers"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:90
#, fuzzy
msgid "Add an emblem to links"
msgstr "Afegir un emblema als enllaços"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:91
#, fuzzy
msgid "Add an emblem to allow to identify soft links."
msgstr "Afegir un emblema per permetre identificar enllaços suaus."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:95
#, fuzzy
msgid "Use black for label text"
msgstr "Utilitzeu el negre per al text de l'etiqueta"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:96
#, fuzzy
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""
"Pintar el text de l'etiqueta en negre en lloc de blanc. Útil a l'hora "
"d'utilitzar fons de llum."

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:100
#, fuzzy
msgid "Show new icons on non primary monitor if connected"
msgstr "Mostra noves icones al monitor no primari si està connectat"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:101
#, fuzzy
msgid ""
"If a second monitor is connected, new icons are placed on the non primary "
"monitor"
msgstr ""
"Si un segon monitor està connectat, es col·loquen noves icones al monitor no "
"primari"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:105
#, fuzzy
msgid "Icons can be positioned anywhere on Desktop"
msgstr "Les icones es poden col·locar en qualsevol lloc de l'escriptori"

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:106
#, fuzzy
msgid ""
"Icons are not on a rectangular grid but can be postioned anywhere "
"independent of grid"
msgstr ""
"Les icones no es troben en una quadrícula rectangular, però es poden penjar "
"a qualsevol lloc independent de la quadrícula"

#~ msgid "Change Background…"
#~ msgstr "Canvia el fons de l'escriptori…"

#~ msgid "Display Settings"
#~ msgstr "Paràmetres de la pantalla"

#, fuzzy
#~ msgid "Rectangular icons with drop grid highlighting"
#~ msgstr "Icones rectangulars amb retolació desplegable"

#, fuzzy
#~ msgid "Close"
#~ msgstr "Tanca"

#, fuzzy
#~ msgid "Use Nemo to open folders"
#~ msgstr "Utilitzeu Nemo per obrir carpetes"

#, fuzzy
#~ msgid "Use Nemo instead of Nautilus to open folders."
#~ msgstr "Utilitzeu Nemo en lloc de Nautilus per obrir carpetes."

#, fuzzy
#~ msgid "Show Desktop In GNOME Files"
#~ msgstr "Escriptori d'espectacle En GNOM Arxiva"

#, fuzzy
#~ msgid "Open In Terminal"
#~ msgstr "Obre al Terminal"

#~ msgid "Open in Terminal"
#~ msgstr "Obre al Terminal"

#, fuzzy
#~ msgid "No Extraction Folder"
#~ msgstr "Sense carpeta d'extracció"

#, fuzzy
#~ msgid "Select Extract Destination"
#~ msgstr "Seleccioneu Extreu la destinació"

#, fuzzy
#~ msgid "No Destination Folder"
#~ msgstr "No hi ha carpeta de destinació"

#~ msgid "Open With Other Application"
#~ msgstr "Obre amb una altra aplicació..."

#~ msgid "Click type for open files"
#~ msgstr "Feu clic al tipus per a obrir fitxers"

#~ msgid "Action to do when launching a program from the desktop"
#~ msgstr "Acció a fer quan s'iniciï un programa des de l'escriptori"

#~ msgid "Display the content of the file"
#~ msgstr "Mostra el contingut del fitxer"

#~ msgid "Launch the file"
#~ msgstr "Executa el fitxer"

#~ msgid "Ask what to do"
#~ msgstr "Preguntar què fer"

#~ msgid "Local files only"
#~ msgstr "Només fitxers locals"

#~ msgid ""
#~ "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
#~ msgstr ""
#~ "El gestor de fitxers Nautilus és obligatori per a treballar amb les "
#~ "icones de l'escriptori NG."

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Per a configurar les icones de l'escriptori NG, feu clic dret a "
#~ "l'escriptori i trieu l'últim element: «Paràmetres de les icones de "
#~ "l'escriptori»"

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "Voleu executar «{0}» o mostrar el seu contingut?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "«{0}» és un fitxer de text executable."

#~ msgid "Execute in a terminal"
#~ msgstr "Obre al Terminal"

#~ msgid "Show"
#~ msgstr "Mostra"

#~ msgid "Execute"
#~ msgstr "Executa"

#~ msgid "New folder"
#~ msgstr "Carpeta nova"

#~ msgid "Delete"
#~ msgstr "Suprimeix"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "Esteu segur que voleu suprimir permanentment aquests elements?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "Si suprimiu un element, es perdrà permanentment."

#, fuzzy
#~ msgid "Desktop Icons Settings"
#~ msgstr "Paràmetres de les icones de l'escriptori"

#~ msgid "Error while deleting files"
#~ msgstr "S'ha produït un error en suprimir els fitxers"
